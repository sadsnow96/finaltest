package com.selenium.test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.Test;
import com.pop.pages.Base;
import com.pop.pages.Home;
import com.pop.pages.Login;
import com.pop.pages.MyAcc;
import com.pop.pages.Search;
import com.pop.pages.contactUs;
import com.pop.pages.product;
import com.pop.pages.order;
import org.testng.annotations.BeforeMethod;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class FinalTest extends Base {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 0)
	public void testPro1() {

		Home home = new Home(driver);
		Login login = new Login(driver);

		home.clickX(home.Login_Link);
		String Wrong_Email = "a";
		login.SignUp(Wrong_Email);

		login.ExWait(30, login.Error_rp);

		String Text = login.getTxt(login.Error_rp);
		Assert.assertTrue(Text.contains("Invalid"));
		System.out.println("testPro1 thanh cong");

	}

	@Test(priority = 1)
	public void testPro2() {
		Home home = new Home(driver);
		Login login = new Login(driver);
		home.clickX(home.Login_Link);

		login.SignUp(email);

		MyAcc myAcc = login.SignUpIn4(FirstName, LastName, pw, Address, City, ZipCode, PhoneNo);
		String Welcome = myAcc.getText_welcome();
		Assert.assertTrue(Welcome.contains("Welcome"));
		System.out.println("testPro2 thanh cong");
	}

	@Test(priority = 2)
	public void testPro3() {
		Home home = new Home(driver);
		home.sender(home.Letter_input, email);
		home.clickX(home.SendLetter_Button);
		home.check_letter();
	}

	@Test(priority = 3)
	public void testPro4() {
		Home home = new Home(driver);
		contactUs contact = new contactUs(driver);
		home.clickX(home.contactUs);
		contact.sendContact(contact.Subject_heading, contact.email_contact, contact.order_contact, contact.fileUpload,
				contact.Message, email, order, fileUpLoad, message);
	}

	@Test(priority = 4)
	public void testPro5() {
		Home home = new Home(driver);
		home.search_sendKeys(search_box, random);
	}

	@Test(priority = 5)
	public void testPro6() {
		Home home = new Home(driver);
		home.check_sugList(search_box, random, home.list_suggest);
	}

	@Test(priority = 6)
	public void testPro61() {
		Home home = new Home(driver);
		product proD = new product(driver);
		proD.test_suggest_result(search_box, home.list_suggest, random);
	}

	@Test(priority = 7)
	public void testPro62() {
		Search search = new Search(driver);
		search.searching(search_box, random);

		String result = search.getTxt(search.resultCount);
		System.out.println(result + "<<<<<<<<<<");

		String resultCut = result.substring(0, 1);
		System.out.println(resultCut);
		float result1 = Float.parseFloat(resultCut);

		search.clearTxt(search_box);
		search.sender(search_box, random);
		List<WebElement> list = driver.findElements(search.searchList);
		float count = list.size();
		System.out.println("size = " + count);

		if (checkcount(count, result1)) {
			System.out.println("dung");
		} else {
			System.out.println("sai");
		}
	}

	@Test(priority = 8)
	public void testPro7() {
		String rp = "No results";
		Search search = new Search(driver);
		search.searching(search_box, "dressie");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String re = search.getTxt(search.alert);
		if(re.contains(rp)) {
			System.out.println("test thanh cong");
		} else {
			System.out.println("test khong thanh cong");
		}
	}

	@Test(priority = 9)
	public void testPro8() {
		Home home = new Home(driver);
		order Order = new order(driver);

		home.hoverman(home.img1);
		home.clickX(home.add1);
		home.clickX(home.continueShoping);
		
		home.hoverman(home.img2);
		home.clickX(home.add2);
		home.clickX(home.continueShoping);
		
		home.hoverman(home.img3);
		home.clickX(home.add3);
		Order.clickX(home.proceed);

		Order.proceedBuy();

	}

	@Test(priority = 10)
	public void testPro9() {
		Home home = new Home(driver);
		order Order = new order(driver);

		home.hoverman(home.img1);
		home.clickX(home.add1);
		home.clickX(home.continueShoping);

		home.hoverman(home.img2);
		home.clickX(home.add2);
		home.clickX(home.continueShoping);

		home.hoverman(home.img3);
		home.clickX(home.add3);
		home.clickX(home.continueShoping);

		home.hoverman(home.img4);
		home.clickX(home.add4);
		home.clickX(home.continueShoping);

		home.hoverman(home.img5);
		home.clickX(home.add5);
		Order.clickX(home.proceed);

		Order.fixQuanty(Order.input_quanty, "3");
		Order.deleteProduct(Order.delete_item);
		String total1 = Order.getTxt(Order.total_price);
		System.out.println("tong gia san pham dung la: " + total1);

		Order.proceedBuy();
		String total2 = Order.getTxt(Order.total_amount);
		System.out.println("tong gia san pham du tinh: " + total2);
		if (checkstring(total1, total2)) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}
	}

	@Test(priority = 11)
	public void testPro10() {
		Home home = new Home(driver);
		String sale = "20%";
		order Order = home.checksale(sale);
		Order.proceedBuy();
	}

	@Test(priority = 12)
	public void testPro11() throws InterruptedException {
		Home home = new Home(driver);
		product proD = new product(driver);
		home.clickX(home.img1);

		int h = proD.getHeight(proD.imgpro1, proD.imgpro1);
		int w = proD.getWidth(proD.imgpro1, proD.imgpro1);

		proD.clickX(proD.imgpro1);
		int h1 = proD.getHeight(proD.bigIMG1, proD.titleIMG);
		int w1 = proD.getWidth(proD.bigIMG1, proD.titleIMG);
		checkZoom(h, w, h1, w1);

		int Yimg = proD.getY(proD.bigIMG1, proD.titleIMG);
		int Ytitle = proD.getY(proD.titleIMG, proD.titleIMG);
		checkUpDown(Yimg, Ytitle);
	}

	@Test(priority = 13)
	public void testPro12() {
		Home home = new Home(driver);
		product proD = new product(driver);
		home.clickX(home.img1);
		proD.ShareTT();
	}

	@Test(priority = 14)
	public void testPro13() {
		Home home = new Home(driver);
		product proD = new product(driver);
		Login login = new Login(driver);
		home.clickX(home.Login_Link);
		MyAcc myAcc = login.login(email, pw);
		myAcc.clickX(myAcc.Home_Link);
		home.clickX(home.img1);
		proD.comment("title", "comment");
	}

	@Test(priority = 15)
	public void testPro14() {
		Home home = new Home(driver);
		product proD = new product(driver);
		home.clickX(home.img1);
		proD.sendFriend("tuan", email);
	}

	@AfterMethod
	public void afterMethod() {
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.close();
	}

}
