package com.pop.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class product extends Base {
	public product(WebDriver driver) {
		this.driver = driver;
	}

	public By namePro = By.xpath("//div/h1[@itemprop='name']");
	// img
	public By imgpro1 = By.xpath("//img[@id = 'bigpic']");
	public By bigIMG1 = By.xpath("//div[@class='fancybox-inner']");
	// title img
	public By titleIMG = By.xpath("//span[@class='child']");
	// twitter
	public By twitter = By.xpath("//button[@class='btn btn-default btn-twitter']");
	// comment
	public By comment = By.xpath("//a[@class='open-comment-form']");
	public By titleCom = By.xpath("//input[@id='comment_title']");
	public By contentCom = By.xpath("//textarea[@id='content']");
	public By sendCom = By.xpath("//button[@id='submitNewMessage']");
	public By alertCom = By.xpath("//div[@class='fancybox-inner']");

	// send to friend
	public By sendFriend = By.xpath("//a[@id='send_friend_button']");
	public By friendName = By.xpath("//input[@id='friend_name']");
	public By friendEmail = By.xpath("//input[@id='friend_email']");
	public By sendEmail = By.xpath("//button[@id='sendEmail']");
	public By alertMail = By.xpath("//div[@class='fancybox-inner']");

	public product test_suggest_result(By searchbox, By listproduct, String a) {
		Home home = new Home(driver);
		product proD = new product(driver);
		sender(search_box, random);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String firstText = home.getFirstTxt();

		home.clickX(home.first_suggest);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		String nameProduct = getTxt(proD.namePro);
		System.out.println(nameProduct);
		if (firstText.contains(nameProduct)) {
			System.out.println("correct");
		} else {
			System.out.println("incorrect");
		}

		sender(search_box, random);
		List<WebElement> list = driver.findElements(home.list_suggest);
		for (int i = 1; i < list.size(); i++) {
			List<WebElement> list1 = driver.findElements(home.list_suggest);
			WebElement xWebElement = list1.get(i);
			String xWebElementGetText = xWebElement.getText();
			System.out.println(xWebElementGetText);
			xWebElement.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			String Productname = getTxt(namePro);

			// kiem tra contains hay khong
			if (xWebElementGetText.contains(Productname)) {
				System.out.println("correct");
			} else {
				System.out.println("incorrect");
			}
			sender(search_box, random);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		return new product(driver);
	}

	public product ShareTT() {
		product proD = new product(driver);
		twitter twit = new twitter(driver);
		proD.clickX(proD.twitter);
		twit.switchToAndBackTo(email, pw);
		return new product(driver);
	}

	// comment
	public product comment(String title, String content) {
		clickX(comment);
		sender(titleCom, title);
		sender(contentCom, content);
		clickX(sendCom);
		if (isElementPresent(alertCom)) {
			System.out.println("comment thanh cong");
		} else {
			System.out.println("comment khong thanh cong");
		}
		return new product(driver);
	}

	// send friend
	public product sendFriend(String friendten, String friendmail) {
		clickX(sendFriend);
		sender(friendName, friendten);
		sender(friendEmail, friendmail);
		clickX(sendEmail);
		if (isElementPresent(alertMail)) {
			System.out.println("send mail thanh cong");
		} else {
			System.out.println("send mail khong thanh cong");
		}
		return new product(driver);
	}

}
