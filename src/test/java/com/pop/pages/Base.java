package com.pop.pages;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
	public WebDriver driver;
	// in4 chung
	public String FirstName = "Tuan";
	public String LastName = "Nguyen";
	public String pw = "12345";
	public String Address = "HaNoi";
	public String City = "HN";
	public String ZipCode = "10000";
	public String PhoneNo = "123451";
	public String email = "tan@gmail.com";
	// contactUs
	public String order = "tuan";
	public String fileUpLoad = "C:\\Users\\LQA\\Downloads\\easyinfo.txt";
	public String message = "tuan";
	// random keyword
	public String random = "Dress";
	// search box
	public By search_box = By.xpath("//*[@id='search_query_top']");
	public By search_button = By.xpath("//*[@id='searchbox']/button");

	// kiem tra 1 phan tu nao do co xuat hien hay khong
	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	// click to an xpath type
	public void clickX(By xpath) {
		ExWait(10,xpath);
		driver.findElement(xpath).click();
	}

	// sendKeys to textbox
	public void sender(By by, String a) {
		driver.findElement(by).sendKeys(a);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// getText
	public String getTxt(By by) {
		ExWait(10,by);
		return driver.findElement(by).getText();
	}

	// search something
	public Search searching(By searchbox, String a) {
		sender(search_box, a);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		clickX(search_button);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return new Search(driver);
	}

	// clear Textbox
	public void clearTxt(By searchbox) {
		driver.findElement(search_box).clear();
	}

	// compare
	public boolean checkcount(float a, float b) {
		if (a == b) {
			return true;
		} else {
			return false;
		}
	}

	// compare
	public boolean checkstring(String a, String b) {
		if (a.contains(b)) {
			return true;
		} else {
			return false;
		}
	}

	// compare
	public void checkUpDown(int a, int b) {
		if (a > b) {
			System.out.println("nam ben duoi");
		} else if (a < b) {
			System.out.println("nam ben tren");
		} else {
			System.out.println("nam cung vi tri");
		}
	}

	// move mouse
	public void hoverman(By img) {
		ExWait(10,img);
		Actions actions = new Actions(driver);
		WebElement Product = driver.findElement(img);
		actions.moveToElement(Product).perform();
	}

	// getsize
	public void getSizeIMG(By img) {
		int h = driver.findElement(img).getSize().getHeight();
		int w = driver.findElement(img).getSize().getWidth();
		System.out.println("height = " + h);
		System.out.println("width = " + w);
	}

	// get Height
	public int getHeight(By img, By condition) {
		//ExWait(10, condition);
		int h = driver.findElement(img).getSize().getHeight();
		System.out.println("width = " + h);
		return h;
	}

	// get width
	public int getWidth(By img, By condition) {
		// product proD = new product(driver);
		//ExWait(10, condition);
		int w = driver.findElement(img).getSize().getWidth();
		System.out.println("width = " + w);
		return w;
	}

	// getPosition
	public void getPosition(By element) {
		Point object = driver.findElement(element).getLocation();
		System.out.println("vi tri cua object =>>>>>>(X,Y) = " + object);
	}

	// getY
	public int getY(By element, By condition) {
		ExWait(10, condition);
		int Y = driver.findElement(element).getLocation().getY();
		System.out.println(Y);
		return Y;
	}

	// getX
	public int getX(By element, By condition) {
		ExWait(10, condition);
		int X = driver.findElement(element).getLocation().getX();
		System.out.println(X);
		return X;
	}

	public void checkZoom(int dai1, int rong1, int dai2, int rong2) {
		if (dai1 < dai2 && rong1 < rong2 || dai1 == dai2 && rong1 < rong2 || dai1 < dai2 && rong1 == rong2) {
			System.out.println("da zoom");
		} else {
			System.out.println("chua zoom");
		}
	}

	// exWait
	public void ExWait(int seconds, By condition) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(condition));
	}
	//imwwait
	public void ImWait(int timeout){
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}

}
