package com.pop.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class order extends Base {
	public order(WebDriver driver) {
		this.driver = driver;
	}

	public By total_price = By.xpath("//span[@id='total_price']");
	public By total_amount = By.xpath("//span[@class='price']");
	public By input_quanty = By.xpath("//input[@size='2']");
	public By delete_item = By.xpath("//i[@class='icon-trash']");

	public By pro_button = By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']");
	public By pro_button1 = By.xpath("//button[@name='processAddress']");
	public By pro_button2 = By.xpath("//button[@name='processCarrier']");
	public By pro_button3 = By.xpath("//button[@class='button btn btn-default button-medium']");
	public By checkbox = By.xpath("//*[@id='uniform-cgv']");
	public By payment = By.xpath("//a[@class='bankwire']");

	public void fixQuanty(By quantybox, String a) {
		List<WebElement> list = driver.findElements(quantybox);
		for (int i = 0; i < 1; i++) {
			WebElement input = list.get(i);
			input.clear();
			input.sendKeys(a);
		}
	}

	public void deleteProduct(By product) {
		List<WebElement> list = driver.findElements(product);
		for (int i = 0; i < 1; i++) {
			list.get(i).click();
		}
	}

	public order proceedBuy() {
		clickX(pro_button);

		Login login = new Login(driver);
		login.login(email, pw);
		clickX(pro_button1);

		clickX(checkbox);
		clickX(pro_button2);

		clickX(payment);
		clickX(pro_button3);
		return new order(driver);
	}

}