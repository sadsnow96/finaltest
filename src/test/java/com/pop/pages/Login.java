package com.pop.pages;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class Login extends Base {
	public Login(WebDriver driver) {
		this.driver = driver;
	}

//Login	
	public By Email_Box = By.xpath("//*[@id=\"email\"]");
	public By Pass_Box = By.xpath("//*[@id=\"passwd\"]");
	public By Login_Button = By.xpath("//*[@id=\"SubmitLogin\"]");
//fail
	public By SignUp_Email_Box = By.xpath("//*[@id=\"email_create\"]");
	public By CreateAcc_Button = By.xpath("//*[@id=\"SubmitCreate\"]");
//invalid
	public By Error_rp = By.xpath("//*[@id=\"create_account_error\"]");
//create acc
	public By FirstName = By.xpath("//*[@id=\"customer_firstname\"]");
	public By LastName = By.xpath("//*[@id=\"customer_lastname\"]");
	public By PassWord = By.xpath("//*[@id=\"passwd\"]");
	public By Address = By.xpath("//*[@id=\"address1\"]");
	public By City = By.xpath("//*[@id=\"city\"]");
	public By Zip = By.xpath("//*[@id=\"postcode\"]");
	public By Phone = By.xpath("//*[@id=\"phone_mobile\"]");
	public By CheckBoxGender = By.xpath("//*[@id=\"id_gender1\"]");
	public By Date = By.xpath("//*[@id=\"days\"]");
	public By Month = By.xpath("//*[@id=\"months\"]");
	public By Year = By.xpath("//*[@id=\"years\"]");
	public By State = By.xpath("//*[@id=\"id_state\"]");
	public By Register_BTN = By.xpath("//*[@id=\"submitAccount\"]");

	public MyAcc login(String a, String b) {
		sender(Email_Box, a);
		sender(Pass_Box, b);
		clickX(Login_Button);
		return new MyAcc(driver);
	}

	public Login SignUp(String a) {
		sender(SignUp_Email_Box, a);
		clickX(CreateAcc_Button);
		return new Login(driver);

	}

	public MyAcc SignUpIn4(String a, String b, String c, String d, String e, String f, String g) {
		clickX(CheckBoxGender);
		sender(FirstName, a);
		sender(LastName, b);
		sender(PassWord, c);

		Select selectdays = new Select(driver.findElement(Date));
		selectdays.selectByIndex(25);
		Select selectmonths = new Select(driver.findElement(Month));
		selectmonths.selectByIndex(2);
		Select selectyears = new Select(driver.findElement(Year));
		selectyears.selectByValue("1998");

		sender(Address, d);
		sender(City, e);

		Select selectstate = new Select(driver.findElement(State));
		selectstate.selectByVisibleText("Texas");

		sender(Zip, f);
		sender(Phone, g);

		clickX(Register_BTN);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return new MyAcc(driver);
	}

}