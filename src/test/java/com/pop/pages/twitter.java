package com.pop.pages;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;

public class twitter extends Base{
	public twitter(WebDriver driver) {
		this.driver = driver;
	}
	
	public void switchToAndBackTo(String id, String pw) {
		String parent = driver.getWindowHandle();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println(parent);
		

		Set<String> handles = driver.getWindowHandles();
		String twitter = "";
		for (String h : handles) {
			if (!h.equals(parent)) {
				twitter = h;
				System.out.println(h);
			}

		}
		try {
			driver.switchTo().window(twitter);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println("Title popup = " + driver.getTitle());		
			driver.findElement(By.xpath("//input[@name='session[username_or_email]']")).sendKeys(id);
			driver.findElement(By.xpath("//input[@name=\"session[password]\"]")).sendKeys(pw);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.close();
			driver.switchTo().window(parent);

		} catch (NoSuchWindowException e) {
			System.out.println("khong the switch");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
}
