package com.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class contactUs extends Base {
	public contactUs(WebDriver driver) {
		this.driver = driver;
	}

	public By Subject_heading = By.xpath("//*[@id=\"id_contact\"]");
	public By email_contact = By.xpath("//*[@id=\"email\"]");
	public By order_contact = By.xpath("//*[@id=\"id_order\"]");
	public By fileUpload = By.xpath("//*[@id=\"fileUpload\"]");
	public By Message = By.xpath("//*[@id=\"message\"]");
	public By Upload_button = By.xpath("//*[@id=\"submitMessage\"]");
	public By success_msg = By.xpath("//*[@class=\"alert alert-success\"]");

	public contactUs sendContact(By subject, By email, By order, By fileUpload, By message, String a, String b,
			String c, String d) {
		Select Subject = new Select(driver.findElement(subject));
		Subject.selectByIndex(1);

		sender(email, a);
		sender(order, b);
		sender(fileUpload, c);
		sender(message, d);
		clickX(Upload_button);
		Assert.assertTrue(isElementPresent(success_msg));
		System.out.println("testPro4 thanh cong");
		return new contactUs(driver);
	}

}
