package com.pop.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Home extends Base {
	public Home(WebDriver driver) {
		this.driver = driver;
	}
//test vui
	public By search_tgdd = By.xpath("//*[@id='skw']");
	public By search_btn = By.xpath("//button[@type='submit']");
	public By title = By.xpath("//h2[contains(text(),'ĐIỆN THOẠI')]");
	public By popup = By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div[2]/div[1]/a");



	public By Login_Link = By.xpath("//div[@class='header_user_info']");
	public By BestSeller_Link = By.xpath("//a[@class='blockbestsellers']");
	public By Letter_input = By.xpath("//*[@id='newsletter-input']");
	public By SendLetter_Button = By.xpath("//button[@name='submitNewsletter']");
	// Newsletter
	public By NewsLetter = By.xpath("//*[@class='alert alert-success']");
	// contactUs
	public By contactUs = By.xpath("//a[@title='Contact us']");
	// suggest
	public By list_suggest = By.xpath("//div[@class='ac_results']/ul/li");
	public By first_suggest = By.xpath("//*[@id='index']/div[2]/ul/li[1]");
	// img
	public By img1 = By.xpath("//ul[@id='homefeatured']//div[@class='product-image-container']//a[@title='Faded Short Sleeve T-shirts']");
	public By add1 = By.xpath("//ul[@id='homefeatured']//div[@class='button-container']//a[@data-id-product='1']");

	public By img2 = By.xpath("//ul[@id='homefeatured']//div[@class='product-image-container']//a[@title='Blouse']");
	public By add2 = By.xpath("//ul[@id='homefeatured']//div[@class='button-container']//a[@data-id-product='2']");

	public By img3 = By.xpath("//ul[@id='homefeatured']//div[@class='product-image-container']//a//img[@src='http://automationpractice.com/img/p/8/8-home_default.jpg']");
	public By add3 = By.xpath("//ul[@id='homefeatured']//div[@class='button-container']//a[@data-id-product='3']");

	public By img4 = By.xpath("//ul[@id='homefeatured']//div[@class='product-image-container']//a//img[@src='http://automationpractice.com/img/p/1/0/10-home_default.jpg']");
	public By add4 = By.xpath("//ul[@id='homefeatured']//div[@class='button-container']//a[@data-id-product='4']");

	public By img5 = By.xpath("//ul[@id='homefeatured']//div[@class='product-image-container']//a//img[@src='http://automationpractice.com/img/p/1/2/12-home_default.jpg']");
	public By add5 = By.xpath("//ul[@id='homefeatured']//div[@class='button-container']//a[@data-id-product='5']");
	//button
	public By continueShoping = By.xpath("//span[@title='Continue shopping']");
	public By proceed = By.xpath("//a[@title=\"Proceed to checkout\"]");
	// list product
	public By list_pro = By.xpath("//*[@id='homefeatured']/li");
	// add to cart when clicked to product
	public By add_cart = By.xpath("//span[normalize-space()='Add to cart']");
	// proceed after add_cart
	public By proceedbuy = By.xpath("//a[@class='btn btn-default button button-medium']");

	public Login clickLogin() {
		driver.findElement(Login_Link).click();
		return new Login(driver);
	}

	public String getFirstTxt() {
		return driver.findElement(first_suggest).getText();
	}

	public Home check_letter() {
		// driver.findElement(Letter_input).sendKeys(a);
		if (isElementPresent(NewsLetter)) {
			System.out.println("testPro3 hoan thanh");
		} else {
			System.out.println("sai email hoac email da duoc su dung");
		}
		return new Home(driver);
	}

	public Home search_sendKeys(By SearchBox, String a) {
		driver.findElement(SearchBox).click();
		driver.findElement(SearchBox).sendKeys(a);
		String Search_Text = driver.findElement(SearchBox).getAttribute("value");
		Assert.assertTrue(!Search_Text.contains("Search"));
		System.out.println("Search is deleted");
		driver.findElement(SearchBox).clear();
		String Default_Text = driver.findElement(SearchBox).getAttribute("placeholder");
		Assert.assertTrue(Default_Text.contains("Search"));
		System.out.println("Search is back");
		return new Home(driver);
	}

	public Home check_sugList(By SearchBox, String a, By sug) {
		driver.findElement(SearchBox).sendKeys(a);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> list = driver.findElements(sug);
		for (int i = 0; i < list.size(); i++) {
			WebElement webElement = list.get(i);
			String webElementText = webElement.getText();
			if (webElementText.contains(random)) {
				System.out.println("goi y dua ra dung voi tu khoa " + random);
			} else {
				System.out.println("goi y dua ra khong dung voi tu khoa " + random);
			}
		}
		return new Home(driver);
	}

	public product firstSuggest_click(By by, String a) {
		sender(by,a);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		clickX(by);
		return new product(driver);
	}

	public order checksale(String salePercent) {
		List<WebElement> list = driver.findElements(list_pro);
		for (int i = 0; i < list.size(); i++) {

			WebElement product = list.get(i);
			String needToBuy = list.get(i).getText();
			System.out.println(needToBuy);
			String sale = salePercent;

			if (needToBuy.contains(sale)) {
				product.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				clickX(add_cart);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				clickX(proceedbuy);
			}
		}
		return new order(driver);
	}

}
