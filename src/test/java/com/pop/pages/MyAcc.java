package com.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class MyAcc extends Base {
	public MyAcc(WebDriver driver) {
		this.driver = driver;
	}
	public By Home_Link = By.xpath("//i[@class=\"icon-chevron-left\"]");
//	public String WelcomeMessage = "Welcome to your account. Here you can manage all of your personal information and orders.";
	public By Welcome = By.xpath("//*[@id=\"center_column\"]/p"); 
	
	public Home HomeReturn() {
		clickX(Home_Link);
		return new Home(driver);
	}

	public String getText_welcome() {
		return getTxt(Welcome);
		
	}
}
