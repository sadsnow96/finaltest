package com.pop.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class Search extends Base {
	public Search(WebDriver driver) {
		this.driver = driver;
	}

	public By resultCount = By.xpath("//span[@class='heading-counter']");
	public By searchList = By.xpath("//img[@class='replace-2x img-responsive'][@height='250']");
	public By alert = By.xpath("//*[@id='center_column']/p");
}
